# encoding:utf-8
class Animal:

    def __init__(self, especie='', nombre='', edad=''):
        self.__especie = especie
        self.__nombre = nombre
        self.__edad = edad

    def __str__(self):
        return "Hola me llamo {} y mi especie es {}".format(self.__nombre, self.__especie)

    def haceRuido(self):
        pass


class Perro(Animal):

    def __init__(self, nombre='', edad=''):
        Animal.__init__(self, 'Perro', nombre, edad)

    def haceRuido(self):
        return "Guau guau...!"


class Vaca(Animal):
    def __init__(self, nombre='', edad=''):
        Animal.__init__(self, 'Vaca', nombre, edad)

    def haceRuido(self):
        return "Muuuuuuu"


class Mamifero:
    def __init__(self):
        pass

    def gestar(self):
        pass


class Gato(Animal, Mamifero):
    def __init__(self, nombre='', edad=''):
        Animal.__init__(self, 'Gato', nombre, edad)

    def haceRuido(self):
        return "Miauuuu"

    def gestar(self):
        return "Tengo 2 meses de gestación"


# fido = Perro(edad='3', nombre='Fido')
# print("{} y hago {}".format(fido, fido.haceRuido()))
#
# lola = Vaca(nombre='Lola', edad='12')
# print("{} y hago {}".format(lola, lola.haceRuido()))
# #
# misifus = Gato(nombre='Misifus', edad=12)
# print(f'{misifus} {misifus.gestar()}')
#
# ##Polimorfismo
# misMascotas = []
# misMascotas.append(fido)
# misMascotas.append(lola)
# misMascotas.append(misifus)
# for m in misMascotas:
#     print m
# =======
# #
# lola = Vaca(nombre='Lola', edad='12')
# # print"{} y hago {}".format(lola, lola.haceRuido())
#
# misifus = Gato(nombre='Misifus', edad=12)
# # print "{}".format(misifus.gestar())
#
# # Polimorfismo
#
# lstAnimales = []
# lstAnimales.append(fido)
# lstAnimales.append(lola)
# lstAnimales.append(misifus)
#
# for a in lstAnimales:
#     print a
#
# menu = """
#     Selecciona el tipo de objeto que deseas crear: \n
#     1.- Perro
#     2.- Vaca
#     3.- Gato
#     4.- Mostrar Todos
#     0.- Salir
#     """
# >>>>>>> 2b1d0003bdbd5d3e95eee0facaae7075fa10046d
