# encoding:utf-8
from Figura import Figura


class Cuadrado(Figura):

    def __init__(self):
        self.CalculaArea()
        self.CalculaPerimetro()
        self.CalculaVolumen()
        self.CalculaPeso()

    def CalculaArea(self):
        print("Calculando el área de un cuadrado")

    def CalculaPerimetro(self):
        print("Calculando el perímetro de un cuadrado")

    def CalculaVolumen(self):
        print("Calculando el Volumen")

    def CalculaPeso(self):
        print("Calculando el peso del cuadrado")
