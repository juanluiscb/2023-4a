from Figura import Figura


class Circulo(Figura):
    def __init__(self, *args, **kwargs):
        self.CalculaArea()
        self.CalculaPerimetro()
        self.CalculaVolumen()
        self.metodoNuevo()

    def CalculaArea(self):
        print('Calculando el área del Circulo')

    def CalculaPerimetro(self):
        print('Calculando el perímetro del Circulo')

    def CalculaVolumen(self):
        print('Calculando el volumen del Circulo')

    def metodoNuevo(self):
        print('Calculando el método nuevo')

    def otroMetodo(self):
        print('este es otro método')
