# encoding:utf-8
from abc import ABCMeta, abstractmethod


class Figura:
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def CalculaArea(self):
        pass

    @abstractmethod
    def CalculaPerimetro(self):
        pass

    @abstractmethod
    def CalculaVolumen(self):
        pass

    @abstractmethod
    def metodoNuevo(self):
        pass

    @abstractmethod
    def getNombreFigura(self):
        pass

    @abstractmethod
    def otroMetodo(self):
        print('otro metodo')
