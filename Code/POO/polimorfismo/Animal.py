# encoding:utf-8
from abc import ABC, abstractmethod


class Animal(ABC):
    __metaclass__ = ABC

    def __init__(self, especie='', nombre=''):
        self.__especie = especie
        self.__nombre = nombre

    def __str__(self):
        return "Hola me llamo {} y soy un {}".format(self.__nombre, self.__especie)

    @abstractmethod
    def haceRuido(self):
        pass


class Perro(Animal):

    def haceRuido(self):
        return "Guau guau...!"


class Vaca(Animal):
    def haceRuido(self):
        return "Muuuuuuu"


class Mamifero(ABC):
    __metaclass__ = ABC

    def __init__(self):
        pass

    @abstractmethod
    def gestar(self):
        pass


class Gato(Animal, Mamifero):
    def haceRuido(self):
        return "Miauuuu"

    def gestar(self):
        return "Tengo 2 meses de gestación"


class Ballena(Mamifero, Animal):
    def __init__(self):
        Animal.__init__(self, especie='Cetaceo', nombre='mobidik')

    def haceRuido(self):
        print('.............')

    def gestar(self):
        return "Tengo 9 meses de gestación"


fido = Perro(especie='Perro', nombre='Fido')
lola = Vaca(nombre='Lola', especie='Vaca')
misifus = Gato(especie='Gato', nombre='Misifus')
ballena = Ballena()

animales = [fido, lola, misifus, ballena]

for a in animales:
    print(f"{a} y hago el ruido: {a.haceRuido()}")
