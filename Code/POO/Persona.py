class Persona:

    def __init__(self, nombre="", apaterno=None, amaterno=None, edad=0):
        self.__nombre = nombre
        self.__apaterno = apaterno
        self.__amaterno = amaterno
        self.__edad = edad
        self.__nacionalidad = 'Mexicana'

    def setNombre(self, nombre=""):
        self.__nombre = nombre

    def getNombre(self):
        return (f'{self.__nombre}')

    def setApaterno(self, apaterno):
        self.__apaterno = apaterno

    def getApaterno(self):
        return (f'{self.__apaterno}')

    def setMaterno(self, amaterno):
        self.__amaterno = amaterno

    def getMaterno(self):
        return (f'{self.__amaterno}')

    def setEdad(self, edad):
        self.__edad = edad

    def __getEdad(self):
        return (self.__edad)

    def obtenerEdad(self):
        return (self.__getEdad())

    def nombreCompleto(self):
        return f'{self.getNombre()} {self.getApaterno()} {self.getMaterno()}'

    def __str__(self):
        return "Hola soy: {} mi edad es {}".format(self.nombreCompleto(), self.obtenerEdad())


profe = Persona(nombre='Juan Luis', edad=41)
profe.setApaterno('Campos')
profe.setMaterno('Barrera')
print(profe)
