# encoding:utf-8
class MiClase:

    def __init__(self):
        self.__soyPrivado = 'Soy privado'
        self.soyPublico = 666

    def miFuncionPublica(self):
        pass

    def __miFuncionPrivada(self, tengoParametros):
        pass

    def getAtributoPrivado(self):
        print(self.__soyPrivado)


class MiOtraClase(MiClase):
    # Yo heredo las funciones de mi padre

    def tengoNuevasFunciones(self):
        print("Soy un nuevo metodo")

    def getAtributosHeredatosPublicos(self):
        print(self.soyPublico)


# Ejemplo de Implementacion

miclase = MiOtraClase()
miclase.getAtributosHeredatosPublicos()
miclase.getAtributoPrivado()
miclase.tengoNuevasFunciones()
